$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport"  id="myViewport" content="width=device-width,initial-scale=1.0">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var tabsBox = $(".tabs"),
  carousel = $(".carousel"),
	scale_carousel = $(".scale_carousel"),
  stickyBox = $(".sticky"),
  primaryNav = $(".primary_nav"),
  stylerForm = $(".styler"),
  matchHeight = $("[data-mh]"),
  fancy = $(".fancybox"),
  rating = $(".rating_stars"),
  modal   = $("[data-modal]"),
  flexslider = $(".flexslider")
  regForm = $(".reg_form");

if(tabsBox.length){
  include("js/easyResponsiveTabs.js");
}

if(stickyBox.length){
  include("js/jquery.sticky.js");
}

if(carousel.length){
  include("js/owl.carousel.js");
}
if(scale_carousel.length){
  include("js/owl.carousel2.js");
}

if(flexslider.length){
  include("js/jquery.flexslider.js");
}

if(stylerForm.length){
  include("js/jquery.formstyler.js");
}

if(matchHeight.length){
  include("js/jquery.matchHeight-min.js");
}
if(modal.length){
  include('js/jquery.arcticmodal.js');
}
if(fancy.length){
  include('js/jquery.fancybox.js');
  include('js/jquery.fancybox-thumbs.js');
}
if(rating.length){
  include('js/jquery.raty.js');
}

if($(".scroll-pane").length){
  include("js/jquery.jscrollpane.js"); 
};
if($(".scroll-mouse").length){
  include("js/jquery.mousewheel.js"); 
};

if($(".fade").length){
  include("js/jquery.cycle.all.js"); 
};


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}

function stripes(){
    var parent = $(".home_carousel"),
        pagBox = parent.find('.owl-controls'),
        parentW  = parent.width(),
        totalItemW = 0;

    
    if (!pagBox.find(".stripe_left").length || !pagBox.find(".stripe_right").length){
      pagBox.append("<div class='stripe_left'></div><div class='stripe_right'></div>");
    }

    pagBox.find(".owl-page").each(function(){
      totalItemW += $(this).outerWidth();
    })

    pagBox.find('[class^=stripe]').width((parentW / 2) - totalItemW - 90);

  }


$(document).ready(function(){

  // ДЕЛАЕМ ИНИТ ФУНКЦИЙ ЗДЕСЬ
  /*------Cкролл--------*/
  if ($('.scroll-mouse').length){
    $('.scroll-mouse').mousewheel();  
  };

  if ($('.scroll-pane').length){
      $('.scroll-pane').jScrollPane();
  }; 
    

  if(carousel.length){
  	carousel.each(function(){
  		var itemsQty = $(this).data("items");
  		$(this).owlCarousel({
  			items: itemsQty 
  		});
    })
  }
  if(scale_carousel.length){
    scale_carousel.owlCarousel2({
      autoWidth:true,
      margin : 37,
      navText: [ ' ', ' ' ],
      nav: true,
      items : 3,
      loop: true
    });
    scale_carousel.parents("body").addClass('scale_fancy_carousel');
  }

  if(stylerForm.length){
    stylerForm.styler();
  }

  if(primaryNav.length){
    primaryNav.find(".menu_item").each(function(){
      if($(this).find(">ul").length){
        $(this).addClass("hasUl")
      }
    })
  }

  primaryNav.on("click", ".hasUl .menu_link", function(event){
    var current = $(this);

    event.preventDefault();

    if(!current.parent().hasClass("active")){
      primaryNav.find(".sub_menu").slideUp();
      primaryNav.find(".menu_item").removeClass("active");
      current.parent().addClass("active").find(">ul").slideDown();
    }
    else{
      current.parent().removeClass("active").find(">ul").slideUp();
    }
  })

  if(stickyBox.length){

    stickyBox.stick_in_parent({
      offset_top: 0,
      inner_scrolling : false
    });

  }
  if(tabsBox.length){
     $(tabsBox).easyResponsiveTabs(); 
  }

  if(fancy.length){
      $(fancy).fancybox({

          prevEffect  : 'none',
          nextEffect  : 'none',
          helpers : {
            thumbs  : { 
              type: 'inside',
              width : 156,
              height  : 202
            }
          }

      });

  }


  if($(".home_carousel").length){
    stripes();
  }


  $(".product_gal_item").click(function(){

    var $this = $(this).find("a"),
        z_adress=$this.data("big-img"),
        index = $this.closest(".owl-item").index();


    $this.parents(".owl-wrapper").find(".product_gal_item").removeClass('toggle');
    $this.parent(".product_gal_item").addClass("toggle");

    $(".main_poduct_img").fadeOut(600, function(){

        $(this).find('img').attr("src", z_adress);
        $(this).find('.main_poduct_lk').attr("data-big", z_adress).fadeIn(1000);
        $(this).find('.main_poduct_lk').attr("href", z_adress);
        $(this).find('.main_poduct_lk').data("index", index);
           
    });



   }); 

  $(".main_poduct_lk").on("click", function(event){
    var current = $(this),
        currentIndex = current.data("index");

      $(".carousel.thumb").find(".owl-item").eq(currentIndex).find("a").trigger("click");

      event.preventDefault();
  })



  //  video iframe
  if($('.click_video').length){
    $('.click_video').live('click',function(){
      var $this = $(this),
        iframe = $this.parent().find(".video_iframe"),
        iframeSrc = iframe.attr("src");

      $this.addClass('clicked');
      $this.find('img').hide();
      iframe.attr("src", iframeSrc + "?autoplay=1");
    });
  }

    //----rating stars-----
   
      if ($('.rating_stars').length){
        $('.rating_stars').each(function(){
          var itemStar = $(this).attr("data-star");
          if(!$(this).hasClass('staticStar')){
            $(this).raty({
              score: function() {
                return $(this).attr('data-star');
              }
            });
          }
          else{
              $(this).raty({ readOnly: true, score: itemStar });    
          }
        });
      }


  // cycle

  if ($('.flexslider').length){
    $('.flexslider').flexslider({
        animation: "fade",
        animationSpeed: 1000,
        controlNav: false,              
        directionNav: false,
        slideshowSpeed: 2000,
        pauseOnHover: true, 
        slideshow: true,
        start: function(slider){
            $('body').removeClass('loading');
        }
    });
  }

  // $('.fade').each(function(){
  //   $(this).cycle({
  //     fx: 'fade'
  //   });
  // })

  function submitAvailable(){
    var required = false;

    regForm.find(".req_inp").each(function(){
      if(!$(this).val()){
        required = true;
      }
      else{
        required = false;
      }
    })

    if(!required && $(".agree_check").is(":checked")){
          regForm.find("button.notActive").removeClass('notActive').addClass("backgr_orange");
    }
    else if(required || !$(".agree_check").is(":checked")){
        regForm.find("button.backgr_orange").removeClass('backgr_orange').addClass("notActive");
    }



  }


  

    $(".req_inp, .agree_check").on('blur focus', submitAvailable);

  

    $(regForm).on("submit", function(){
      if (regForm.find("button").hasClass('notActive')){
        return false;
      }
    })

  

  //-----------------modal start--------
    if(modal.length){
      modal.on("click", function(event){
        var modalWindow = $(this).data("modal");

        $(modalWindow).arcticmodal();

        event.preventDefault();
      })
    }
  //-----------------modal end--------

  // focus on input
   $(".header_searh_field").live("focus blur", function(e){
      var $this = $(this),
          parentInput = $(".header_search");
      setTimeout(function(){
        $this.toggleClass("focused_child", $this.is(":focus"));
        $this.parent(parentInput).toggleClass("focused_parent", $this.is(":focus"));
      }, 0);
    });


   jQuery('.add-to-card').on('click', function (event) {
        var cart2 = jQuery('.header_cart')

        var imgtodrag2 = jQuery(this).closest(".product_item").find(".product_item_img img"); 
        if (imgtodrag2) {
            var imgclone = imgtodrag2.clone()
                .offset({
                top: imgtodrag2.offset().top,
                left: imgtodrag2.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'z-index': '100'
            })
                .appendTo(jQuery('body'))
                .animate({
                'top': cart2.offset().top + 10,
                    'left': cart2.offset().left + 10,
                    'height': 75,
                    'width': 75
            }, 1000);
            

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                jQuery(this).detach()
            });
        }
        event.preventDefault();
    });


  


})



$(window).resize(function(){
  stripes();

})



